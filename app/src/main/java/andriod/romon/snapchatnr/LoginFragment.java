package andriod.romon.snapchatnr;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private EditText usernameField;
    private EditText passwordField;

    public LoginFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button button = (Button) view.findViewById(R.id.loginButton);
        usernameField = (EditText) view.findViewById(R.id.usernameField);
        passwordField = (EditText) view.findViewById(R.id.passswordField);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameField.getText().toString();
                String password = passwordField.getText().toString();

                Backendless.UserService.login(username, password, new AsyncCallback<BackendlessUser>() {
                    @Override
                    public void handleResponse(BackendlessUser response) {
                        Toast.makeText(getActivity(), "You logged in boi", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(), " Bro you didn't log in", Toast.LENGTH_SHORT).show();
                    }
                },
                true);
            }
        });

        //String[] addFriend = {"add friend"};

        //ListView listView = (ListView) view.findViewById(R.id.Login);
        //ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
          //      getActivity(),
            //    android.R.layout.simple_list_item_1,
              //  addFriend
        //);

        //listView.setAdapter(listViewAdapter);

        //listView.setOnItemClickListener(
          //      new AdapterView.OnItemClickListener() {
            //        @Override
              //      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //        if (position == 0) {
                  //          Intent intent = new Intent(getActivity(), AddFriendsActivity.class);
                    //        startActivity(intent);
                      //  }
                    //}
                //});


        return view;
    }

}
