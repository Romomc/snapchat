package andriod.romon.snapchatnr;


public class FriendRequest {

    private String toUser;
    private String fromUser;
    private boolean accepted;

    public FriendRequest(){
        toUser = "";
        fromUser = "";
        accepted = false;
    }

    public String getToUser(){
        return toUser;
    }

    public String setToUser(String user){
        return toUser = user;
    }

    public String getFromUser(){
        return fromUser;
    }

    public String setFromUser(String user){
        return fromUser = user;
    }

    public boolean isAccepted(){
        return isAccepted();
    }

    public boolean setAccepted(boolean isAccepted){
        accepted = isAccepted;
    }
}
