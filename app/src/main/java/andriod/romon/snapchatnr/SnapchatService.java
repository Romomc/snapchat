package andriod.romon.snapchatnr;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.List;

public class SnapchatService extends IntentService {

    public SnapchatService() {
        super("SnapchatService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if(action.equals(Constants.ACTION_ADD_FRIEND)){
                String firstUserName = intent.getStringExtra("firstUserName");
                String secondUserName = intent.getStringExtra("secondUserName");
            } else if (action.equals(Constants.ACTION_SEND_FRIEND_REQUEST)){
                String toUser = intent.getStringExtra("toUser");
                String fromUser = intent.getStringExtra("fromUser");
            }
        }
    }

    private void addFriends(String firstUserName, String secondUserName){
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("name = %s or name = %s", firstUserName, secondUserName));

        Backendless.Persistence.of(BackendlessUser.class).find(query, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                List<BackendlessUser> users = response.getData();
                if(users.size() != 2){
                    broadcastAddFriendFailure();
                }else{
                    BackendlessUser user1 = users.get(0);
                   final BackendlessUser user2 = users.get(1);

                    updateFriendsList(user1, user2);
                    Backendless.UserService.update(user1, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser user) {
                            updateFriendsList(user2, user);
                            Backendless.UserService.update(user2, new AsyncCallback<BackendlessUser>() {
                                @Override
                                public void handleResponse(BackendlessUser response) {
                                    broadcastAddFriendSuccess();
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    broadcastAddFriendFailure();
                                }
                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                           broadcastAddFriendSuccess();
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                broadcastAddFriendFailure();
            }
        });
    }

    private void broadcastAddFriendSuccess(){
        Intent intent = new Intent(Constants.BROADCAST_ADD_FRIEND_SUCCESS);
        sendBroadcast(intent);
    }

    private void broadcastAddFriendFailure(){
        Intent intent = new Intent(Constants.BROADCAST_ADD_FRIEND_FAILURE);
        sendBroadcast(intent);
    }

    private void updateFriendsList(BackendlessUser user, BackendlessUser friend){
        BackendlessUser[] newFriends;

        Object[] currentFriendObject = (Object[]) user.getProperty("friends");
        if(currentFriendObject.length > 0) {
            BackendlessUser[] currentFriends = (BackendlessUser[]) currentFriendObject;
            newFriends = new BackendlessUser[currentFriends.length = + 1];
            for(int i = 0; i < currentFriends.length; i++){
                newFriends[i] = currentFriends[1];
            }
            newFriends[newFriends.length - 1] = friend;

        }else{
            newFriends = new BackendlessUser[] { friend };
        }

        user.setProperty("friends", newFriends);
    }

}
