package andriod.romon.snapchatnr;

public class Constants {

    public static final String ACTION_ADD_FRIEND = "android.romon.snapchatnr.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "android.romon,snapchatnr.SEND_FRIEND_REQUEST";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "android.romon.snaochatnr.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "android.romon.snaochatnr.ADD_FRIEND_FAILURE";
}
