package andriod.romon.snapchatnr;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {

    private ArrayList<String> friends;
    private ArrayAdapter<String> friendsListAdapter;


    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        friends = new ArrayList<String>();
        friendsListAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, friends);

        ListView friendsList = (ListView) view.findViewById(R.id.myFriends);
        friendsList.setAdapter(friendsListAdapter);

        String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Object[] friendsObject = (Object[]) user.getProperty("friends");
                if(friendsObject.length > 0){
                    BackendlessUser[] friendsArray = (BackendlessUser[]) friendsObject;
                    for(BackendlessUser friend : friendsArray){
                        String name = friend.getProperty("name").toString();
                        friends.add(name);
                        friendsListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });

        //String[] fProfile = {"Friends Profile"};

        //ListView listView = (ListView) view.findViewById(R.id.myFriends);
       // ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
              //  getActivity(),
                //android.R.layout.simple_list_item_1,
                //fProfile
        //);

        //listView.setAdapter(listViewAdapter);

       // listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //@Override
           // public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
         //       if (position == 0) {
              //      Intent intent = new Intent(getActivity(), FriendsProfileActivity.class);
                //    startActivity(intent);
                //}
            //}
        //});
        return view;
    }

}
