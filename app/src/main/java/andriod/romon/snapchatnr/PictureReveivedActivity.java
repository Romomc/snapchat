package andriod.romon.snapchatnr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PictureReveivedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_reveived);

        PicturesReceivedFragment pictures = new PicturesReceivedFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.um, pictures).commit();
    }
}
