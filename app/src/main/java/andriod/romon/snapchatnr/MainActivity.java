package andriod.romon.snapchatnr;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity{

    public static final String APP_ID = "7B4CA9E1-6DA9-8198-FF33-CF1DFE3F0B00";
    public static final String SECRET_KEY = "D4E58DD2-CB17-1397-FF47-BA30A09D7400";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Backendless.initApp(this, APP_ID,SECRET_KEY,VERSION );
        if(Backendless.UserService.loggedInUser() == ""){
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();


        }else{
            ScreenFragment screen = new ScreenFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, screen).commit();
        }
    }
}
