package andriod.romon.snapchatnr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AddFriendsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friends);

        AddFriendsFragment addPerson = new AddFriendsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.ok, addPerson).commit();
    }
}
